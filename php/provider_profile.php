<?php
include_once '../include_once/connection.php';

$id = $_POST['providerid'];

$info = mysqli_query($con,"Select * from tbl_serviceproviders where ProviderID = '$id'");
$desc = mysqli_query($con,"Select * from tbl_profile where ClientID = '$id'");

$get_info = mysqli_fetch_array($info);
$get_desc = mysqli_fetch_array($desc);

$response = array();

$response['providerid'] = $get_info['ProviderID'];
$response['name'] = $get_info['Lastname'].', '.$get_info['Firstname'].' '.$get_info['Middlename'];
$response['address'] = $get_info['Address'].' '.$get_info['CityTown'].' Laguna';
$response['email'] = $get_info['Email'];
$response['contact'] = $get_info['ContactNo'];

$response['desc'] = $get_desc['Description'];

echo json_encode($response);
?>
<?php
include_once '../include_once/connection.php';

$history = $_POST['historyid'];
$rating = $_POST['rating'];
$feedback = $_POST['feedback'];

$query = mysqli_query($con,"Select * from tbl_appointments where ID = '$history'");
$row = mysqli_fetch_array($query);

$response = array();

if($row['Status'] == 'Completed'){
	$response['status'] = true;
	
	$check = mysqli_query($con,"Select * from tbl_reviews where AppointmentID = '$history'");
	$count = mysqli_num_rows($check);
	
	if($count > 0){
		$response['status'] = false;
		$response['message'] = "You can only give one feedback per transaction.";
	}else{
		
		$insert = mysqli_query($con,"Insert into tbl_reviews(AppointmentID,Review,Rating) values ('$history','$feedback','$rating')");
		
		if($insert){
			$response['status'] = true;
			$response['message'] = "Thank you for giving your feedback.";	
		}else{
			$response['status'] = false;
			$response['message'] = "Unable to add your feedback";
		}
	}
}else{
	$response['status'] = false;
	$response['message'] = "You can only add your feedback if the status of your appointment is completed.";
}

echo json_encode($response);
?>
<?php
include_once '../include_once/connection.php';

$type = $_POST['type'];

$response = array();

if($type == "Provider"){
	$id = $_POST['provider_id'];
	
	$query = mysqli_query($con,"Select * from tbl_serviceproviders where ProviderID = '$id'");
	$row = mysqli_fetch_array($query);
	
	$query1 = mysqli_query($con,"Select * from tbl_profile where ClientID = '$id'");
	$row1 = mysqli_fetch_array($query1);
	
	$response['address'] = $row['Address'];
	$response['contact'] = $row['ContactNo'];
	$response['desc'] = $row1['Description'];		
}else if($type == "Client"){
	$id = $_POST['client_id'];
	$query = mysqli_query($con,"Select * from tbl_clients where ClientID = '$id'");
	$row = mysqli_fetch_array($query);
	
	$response['fname'] = $row['Firstname'];
	$response['mname'] = $row['Middlename'];
	$response['lname'] = $row['Lastname'];
	$response['bday'] = $row['Birthday'];
	$response['address'] = $row['Address'];
	$response['contact'] = $row['ContactNo'];
}

echo json_encode($response);

?>
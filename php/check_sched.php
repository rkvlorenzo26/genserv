<?php
include_once '../include_once/connection.php';

$prov_id = $_POST['prov_id'];
$client_id = $_POST['client_id'];
$date = $_POST['date'];
$time = $_POST['time'];
$problem = $_POST['problem'];

$provider = mysqli_query($con,"Select * from tbl_serviceproviders where ProviderID = '$prov_id'");
$row = mysqli_fetch_array($provider);

$check_sched = mysqli_query($con,"Select * from tbl_appointments where ProviderID = '$prov_id' AND Date = '$date' AND Time = '$time'");
$count_check_sched = mysqli_num_rows($check_sched);

$check_if_appoint = mysqli_query($con,"Select * from tbl_appointments where ProviderID = '$prov_id' AND ClientID = '$client_id' AND Date = '$date'");
$count_check_if_appoint = mysqli_num_rows($check_if_appoint);

$response = array();

if($count_check_sched > 0){
	$response['status'] = false;
	$response['message'] = "Schedule is not available.";
}else if($count_check_if_appoint > 0){
	$response['status'] = false;
	$response['message'] = "You already set a schedule to this provider.";
}else{
	
	$insert = mysqli_query($con,"Insert into tbl_appointments(ProviderID,ClientID,ServiceID,Problem,Date,Time,Status) values ('$prov_id','$client_id','$row[ServiceID]','$problem','$date','$time','Pending')");
	if($insert){
		$response['status'] = true;
		$response['message'] = "Appointment already set. Wait for the confirmation of Service Provider.";
	}
}

echo json_encode($response);

?>
<?php
include_once 'include_once/connection.php';
include_once 'include_once/header.php';
?>

  <body>

  <section id="container" >
      <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
      <!--header start-->
      
      <?php include_once 'include_once/navbar.php' ?>
      
      <!-- header end -->
      
      <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
      <!--sidebar start-->
      
      <?php include_once 'include_once/sidebar.php'; ?>
      
      <!--sidebar end-->
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper site-min-height">
          	<h3><i class="fa fa-angle-right"></i> Clients</h3>
          	<div class="row mt">
          		<div class="col-lg-12">
                    <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                              <th width="2%">ID</th>
                              <th width="25%">Name</th>    
                              <th width="20%">Address</th>
                              <th width="10%">City/Town</th>
                              <th width="10%">Email Address</th>
                              <th width="10%">Contact No.</th>
                              <th width="10%">Date Registered</th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php
                                
                                $clients = mysqli_query($con,"Select * from tbl_clients");
                                
                                while($row = mysqli_fetch_array($clients)){
                                ?>
                                    <tr>
                                    <td><?php echo $row['ClientID']; ?></td>
                                    <td><?php echo $row['Lastname'].', '.$row['Firstname'].' '.$row['Middlename']; ?></td>
                                    <td><?php echo $row['Address'];?></td>
                                    <td><?php echo $row['CityTown']; ?></td>
                                    <td><?php echo $row['Email']; ?></td>
                                    <td><?php echo $row['ContactNo']; ?></td>
                                    <td><?php echo $row['DateRegistered']; ?></td>
                                    </tr>
                                
                                <?php
                                }
                                ?>
                            </tbody>
                            </table>
                        </div>
          		</div>
          	</div>
			
		</section><! --/wrapper -->
      </section><!-- /MAIN CONTENT -->

      <!--main content end-->
      
      <!--footer start-->
      <?php
      include_once 'include_once/footer.php';
      ?>
      <!--footer end-->
      
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
    <?php include_once 'include_once/js.php'; ?>

  </body>
</html>

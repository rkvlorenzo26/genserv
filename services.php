<?php
include_once 'include_once/connection.php';
include_once 'include_once/header.php';
?>

  <body>

  <section id="container" >
      <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
      <!--header start-->
      
      <?php include_once 'include_once/navbar.php' ?>
      
      <!-- header end -->
      
      <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
      <!--sidebar start-->
      
      <?php include_once 'include_once/sidebar.php'; ?>
      
      <!--sidebar end-->
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper site-min-height">
          	<h3><i class="fa fa-angle-right"></i> List of Services</h3>
            
            <?php  
            if(isset($_GET['message'])){
                $message = $_GET['message'];
                
                if($message == 'add_success'){
                ?>
                    <div class="alert alert-info alert-dismissible" id="success-alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo "Successfully added new service."; ?>
                    </div>  
                <?php
                }else if($message == 'add_failed'){
                ?>
                    <div class="alert alert-danger alert-dismissible" id="success-alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo "Service already exists."; ?>
                    </div>  
                <?php    
                }else if($message == 'edit_success'){
                ?>
                    <div class="alert alert-info alert-dismissible" id="success-alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo "Successfully edited service."; ?>
                    </div>
                <?php
                }else if($message == 'edit_failed'){
                ?>
                    <div class="alert alert-danger alert-dismissible" id="success-alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo "Service already exists."; ?>
                    </div>
                <?php
                }
            }
                
            ?>
            
              
            <button class="btn btn-primary pull-right" data-toggle="modal" data-target='#newservice'>New Service</button>  
            <br>  
          	<div class="row mt">
          		<div class="col-lg-12">
          		    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th width="15%">ID</th>
                            <th>Service</th>
                            <th  width="15%">Action</th>
                         </tr>
                        </thead>
                        <tbody>
                            <?php
                                
                            $services = mysqli_query($con,"Select * from tbl_services");
                                
                            while($row = mysqli_fetch_array($services)){
                            ?>
                                <tr>
                                <td><?php echo $row['ID']; ?></td>
                                <td><?php echo $row['Service'];?></td>
                                <td><center><button class="btn btn-primary btn-xs" data-toggle="modal" data-target="<?php echo "#".$row['ID'] ?>"><i class="fa fa-pencil"></i> Edit</button></center></td>
                                </tr>
                                
                            <div class="modal fade" id="<?php echo $row['ID'] ?>">
                                <div class="modal-dialog modal-md">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button class="close" data-dismiss="modal">×</button>
                                            <h4 class="modal-title">Edit Service</h4>
                                        </div><!-- end modal-header -->
                                        <div class="modal-body">
                                            <center>
                                            <form method="POST" action="function/service.php">
                                                <div>
                                                <h5 class="pull-left">Service</h5>
                                                <input type="hidden" name="action" value="Edit">    
                                                <input class="form-control" type="text" name="service" value="<?php echo $row['Service']; ?>" required autocomplete="off">
                                                <input type="text" name="id" value="<?php echo $row['ID']; ?>" hidden="hidden">
                                                </div>
                                                <br>
                                                <input type="submit" placeholder="Enter Service Here" name="submit" class="btn btn-primary pull-right">
                                                <br><br> 
                                            </form>  
                                            </center>
                                        </div><!-- end modal-content -->
                                    </div><!-- end modal-dialog -->
                                </div>
                            </div>  
                            <?php
                            }
                            ?>
                        </tbody>
                    </table>
          		</div>
          	</div>
			
		</section><! --/wrapper -->
      </section><!-- /MAIN CONTENT -->

      <!--main content end-->
      
      
      
      <div class="modal fade" id="newservice">
           <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <button class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">New Service</h4>
                    </div><!-- end modal-header -->
                    <div class="modal-body">
                        <form method="POST" action="function/service.php">
                            <input type="hidden" name="action" value="Add">
                            <div class="form-group">
                                <label>Service:</label>
                                <input type="text" name="service" class="form-control" placeholder="Enter Service Here" required autocomplete="off">
                            </div>
                            <input type="submit" name="submit" class="btn btn-primary pull-right">
                            <br><br>
                        </form>    
                    </div>
               </div>
          </div>
      </div>
      
      
      
      
      
      
      
      
      
      <!--footer start-->
      <?php
      include_once 'include_once/footer.php';
      ?>
      <!--footer end-->
      
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
    <?php include_once 'include_once/js.php'; ?>

  </body>
</html>

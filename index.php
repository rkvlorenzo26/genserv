<?php
include_once 'include_once/connection.php';
include_once 'include_once/header.php';

session_start();

if(!isset( $_SESSION['adminloggedin'])){
  header("location: login.php");
}


$providers = mysqli_query($con,"Select * from tbl_serviceproviders");
$count_providers = mysqli_num_rows($providers);

$clients = mysqli_query($con,"Select * from tbl_clients");
$count_clients = mysqli_num_rows($clients);

$services = mysqli_query($con,"Select * from tbl_services");
$count_services = mysqli_num_rows($services);

?>

  <body>

  <section id="container" >
      <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
      <!--header start-->
      
      <?php include_once 'include_once/navbar.php' ?>
      
      <!-- header end -->
      
      <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
      <!--sidebar start-->
      
      <?php include_once 'include_once/sidebar.php'; ?>
      
      <!--sidebar end-->
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper site-min-height">
          	<h3><i class="fa fa-angle-right"></i> Dashboard</h3>
          	<div class="row mt">
          		<div class="col-lg-12">
                    <a href="providers.php">
          		    <div class="col-md-4 col-sm-4 mb">
                      		<div class="darkblue-panel pn">
                      			<div class="darkblue-header">
						  			<h5>No. of Registered Service Providers</h5>
                      			</div>
                      			<h1 class="mt"><i class="fa fa-users fa-3x"></i></h1>
								<h2><?php echo $count_providers; ?></h2>
                      		</div><!-- -- /darkblue panel ---->
                    </div>
                    </a>    
                    
                    <a href="clients.php">
                    <div class="col-md-4 col-sm-4 mb">
                      		<div class="darkblue-panel pn">
                      			<div class="darkblue-header">
						  			<h5>No. of Registered Clients</h5>
                      			</div>
                      			<h1 class="mt"><i class="fa fa-users fa-3x"></i></h1>
								<h2><?php echo $count_clients; ?></h2>
                      		</div><!-- -- /darkblue panel ---->
                    </div>
                    </a>
                    
                    <a href="services.php">
                    <div class="col-md-4 col-sm-4 mb">
                      		<div class="darkblue-panel pn">
                      			<div class="darkblue-header">
						  			<h5>No. of Services</h5>
                      			</div>
                      			<h1 class="mt"><i class="fa fa-tasks fa-3x"></i></h1>
								<h2><?php echo $count_services; ?></h2>
                      		</div><!-- -- /darkblue panel ---->
                    </div>
                    </a>
          		</div>
          	</div>
			
		</section><! --/wrapper -->
      </section><!-- /MAIN CONTENT -->

      <!--main content end-->
      
      <!--footer start-->
      <?php
      include_once 'include_once/footer.php';
      ?>
      <!--footer end-->
      
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
    <?php include_once 'include_once/js.php'; ?>

  </body>
</html>

      <!--sidebar start-->
      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion">
              	  
                  <li>
                      <a href="index.php">
                          <i class="fa fa-dashboard"></i>
                          <span>Dashboard</span>
                      </a>
                  </li>
                  
                  <li >
                      <a href="providers.php">
                          <i class="fa fa-users"></i>
                          <span>Service Providers</span>
                      </a>
                  </li>
                  
                  <li >
                      <a href="clients.php">
                          <i class="fa fa-users"></i>
                          <span>Clients</span>
                      </a>
                  </li>
                  <li>
                    <a href="reviews.php">
                        <i class="fa fa-star"></i>
                        <span>Reviews</span>
                    </a>
                  </li>
                  <li >
                      <a href="services.php">
                          <i class="fa fa-tasks"></i>
                          <span>List of Services</span>
                      </a>
                  </li>
                  <li>
                      <a href="reports.php">
                          <i class="fa fa-bar-chart-o"></i>
                          <span>Reports</span>
                      </a>
                  </li>
              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->
<?php
include_once 'include_once/connection.php';
include_once 'include_once/header.php';
?>

  <body>

  <section id="container" >
      <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
      <!--header start-->
      
      <?php include_once 'include_once/navbar.php' ?>
      
      <!-- header end -->
      
      <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
      <!--sidebar start-->
      
      <?php include_once 'include_once/sidebar.php'; ?>
      
      <!--sidebar end-->
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper site-min-height">
          	<h3><i class="fa fa-angle-right"></i> Reviews</h3>
          	<div class="row mt">
          		<div class="col-lg-12">
                    <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                              <th width="2%">Appointment ID</th>
                              <th width="15%">Service Provider Name</th>    
                              <th width="15%">Client Name</th>
                              <th width="8%">Service</th>
                              <th width="10%">Date Reviewed</th>
                              <th width="2%">Rating</th>
                              <th width="48%">Feedback</th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php
                                
                                $reviews = mysqli_query($con,"Select * from tbl_reviews");
                                
                                while($row = mysqli_fetch_array($reviews)){
                                    $appointments = mysqli_query($con,"Select * from tbl_appointments where ID = '$row[AppointmentID]'");
                                    $a = mysqli_fetch_array($appointments);

                                    $provider = mysqli_query($con,"Select * from tbl_serviceproviders where ProviderID = '$a[ProviderID]'");
                                    $b = mysqli_fetch_array($provider);

                                    $client = mysqli_query($con, "Select * from tbl_clients where ClientID = '$a[ClientID]'");
                                    $c = mysqli_fetch_array($client);

                                    $service = mysqli_query($con,"Select * from tbl_services where ID = '$a[ServiceID]'");
                                    $d = mysqli_fetch_array($service);
                                ?>
                                    <tr>
                                    <td><?php echo $row['AppointmentID']; ?></td>
                                    <td><?php echo $b['Lastname'].', '.$b['Firstname'].' '.$b['Middlename']; ?></td>
                                    <td><?php echo $c['Lastname'].', '.$c['Firstname'].' '.$c['Middlename']; ?></td>
                                    <td><?php echo $d['Service']; ?></td>
                                    <td><?php echo $row['DateReviewed']; ?></td>
                                    <td><center><?php echo $row['Rating']; ?> <span class="fa fa-star"></span></center></td>
                                    <td><?php echo $row['Review']; ?></td>
                                    </tr>
                                
                                <?php
                                }
                                ?>
                            </tbody>
                            </table>
                        </div>
          		</div>
          	</div>
			
		</section><! --/wrapper -->
      </section><!-- /MAIN CONTENT -->

      <!--main content end-->
      
      <!--footer start-->
      <?php
      include_once 'include_once/footer.php';
      ?>
      <!--footer end-->
      
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
    <?php include_once 'include_once/js.php'; ?>

  </body>
</html>

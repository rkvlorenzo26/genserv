-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 27, 2018 at 06:47 PM
-- Server version: 5.7.22-log
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_genserv`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE IF NOT EXISTS `tbl_admin` (
  `ID` int(11) NOT NULL,
  `Username` varchar(32) NOT NULL,
  `Password` varchar(32) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`ID`, `Username`, `Password`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_appointments`
--

CREATE TABLE IF NOT EXISTS `tbl_appointments` (
  `ID` int(11) NOT NULL,
  `ProviderID` int(11) DEFAULT NULL,
  `ClientID` int(11) DEFAULT NULL,
  `ServiceID` int(11) NOT NULL,
  `Problem` text NOT NULL,
  `Date` date DEFAULT NULL,
  `Time` time DEFAULT NULL,
  `Status` varchar(20) DEFAULT NULL,
  `DateRequest` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_appointments`
--

INSERT INTO `tbl_appointments` (`ID`, `ProviderID`, `ClientID`, `ServiceID`, `Problem`, `Date`, `Time`, `Status`, `DateRequest`) VALUES
(5, 1, 1, 6, 'CCTV installation.', '2018-04-16', '10:00:00', 'Completed', '2018-04-16 00:47:43'),
(6, 2, 1, 2, 'Sample problem', '2018-04-17', '08:00:00', 'Completed', '2018-04-16 01:28:52'),
(7, 1, 1, 6, 'Sirang CCTV', '2018-04-15', '08:00:00', 'Completed', '2018-04-16 09:57:38'),
(8, 3, 1, 2, 'Baradong Poso Negro', '2018-04-20', '08:00:00', 'Cancelled', '2018-04-17 07:48:41'),
(9, 1, 4, 6, 'Ayaw magbukas ng Cpu', '2018-04-20', '09:00:00', 'Cancelled', '2018-04-18 01:55:53'),
(10, 1, 4, 6, 'Ayaw magbukas ng Cpu', '2018-04-27', '13:00:00', 'Completed', '2018-04-18 01:56:45'),
(11, 1, 4, 6, 'blue screen of death', '2018-04-23', '11:00:00', 'Completed', '2018-04-22 04:24:56'),
(12, 12, 4, 5, 'Sira yung tv namin may natulog sa loob', '2018-04-26', '11:00:00', 'Pending', '2018-04-23 06:52:34'),
(13, 2, 1, 2, 'Sirang poso negro', '2018-04-26', '08:00:00', 'Pending', '2018-04-24 07:04:14'),
(14, 1, 1, 6, 'Unable to open my PC.', '2018-04-25', '08:00:00', 'Completed', '2018-04-24 07:09:43'),
(15, 7, 4, 4, 'pwede bang magpagawa ng bagong lamesang kahoy.', '2018-04-26', '15:00:00', 'Completed', '2018-04-24 14:45:41'),
(16, 20, 34, 1, 'Electrical circuits problem', '2018-04-26', '12:00:00', 'Completed', '2018-04-26 02:47:09'),
(17, 20, 35, 1, 'vbk', '2018-04-23', '10:00:00', 'Approved', '2018-04-26 08:55:39'),
(18, 7, 36, 4, 'sira ang bahay', '2018-06-06', '08:00:00', 'Pending', '2018-05-17 10:37:34');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_clients`
--

CREATE TABLE IF NOT EXISTS `tbl_clients` (
  `ClientID` int(11) NOT NULL,
  `Lastname` varchar(50) NOT NULL,
  `Firstname` varchar(50) NOT NULL,
  `Middlename` varchar(50) NOT NULL,
  `Birthday` date NOT NULL,
  `Address` text NOT NULL,
  `CityTown` varchar(50) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `ContactNo` varchar(20) NOT NULL,
  `RecoveryEmail` varchar(255) NOT NULL,
  `Password` varchar(32) NOT NULL,
  `DateRegistered` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_clients`
--

INSERT INTO `tbl_clients` (`ClientID`, `Lastname`, `Firstname`, `Middlename`, `Birthday`, `Address`, `CityTown`, `Email`, `ContactNo`, `RecoveryEmail`, `Password`, `DateRegistered`) VALUES
(1, 'Lorenzo', 'Ralph Kenneth', 'Valencia', '1995-03-26', '316 San Jose St.', 'Binan', 'rkvl0326@gmail.com', '09152664843', 'rkvlorenzo26@yahoo.com.ph', 'dd4b21e9ef71e1291183a46b913ae6f2', '2018-04-06 09:48:38'),
(2, 'Lorenzo', 'Arken', 'Valencia', '1995-03-26', '372 @Unit 4 San Jose St.', 'Calamba', 'arken@gmail.com', '09152664843', 'arken@gmail.com', '25d55ad283aa400af464c76d713c07ad', '2018-04-12 09:59:19'),
(3, 'Dela Cruz', 'Juan', 'Reyes', '1995-01-01', 'Brgy. San Vicente', 'Binan', 'skywalker@gmail.com', '09067919241', 'skywalker@gmail.com', 'dd4b21e9ef71e1291183a46b913ae6f2', '2018-04-16 10:24:12'),
(4, 'Sumilang', 'Lawrence', 'Rebong', '1998-03-27', 'T. Franco St Victoria,Laguna', 'Alaminos', 'lrsumilang@gmail.com', '09272974318', 'lrsumilang@gmail.com', '4d52e630b7a6eca1c6eb0d6b494904ae', '2018-04-18 01:52:53'),
(5, 'Flores', 'Ivy', 'Sia', '1997-03-02', 'Bagumbayan', 'Santa Cruz', 'ivyflores@yahoo.com', '09356506941', 'ivyflores@yahoo.com', 'c4263dabbb0a01bbfae85b20976f3833', '2018-04-19 05:10:31'),
(30, 'elona', 'rey', 'regaspi', '1998-03-21', 'pila', 'Pila', 'reyelona@gmail.com', '09752615870', 'reyelona@gmail.com', '0d267d23b0bf0b646253e1ecb28bdb65', '2018-04-24 05:37:56'),
(32, 'Sumilang', 'Lawrence', 'Rebong', '1998-03-27', 'T. Franco St. Victoria, Laguna', 'Victoria', 'lrsumilang@gmail', '09355240255', 'lrsumilang@gmail', '4d52e630b7a6eca1c6eb0d6b494904ae', '2018-04-24 07:06:35'),
(33, 'Tanawan', 'Darwin', 'Ombico', '1998-04-04', 'brgy san juan', 'Santa Cruz', 'josephdarwin_08@yahoo.com', '09231546798', 'josephdarwin_08@yahoo.com', 'f86f65d3a6095966c8383a33474d5fb6', '2018-04-25 16:45:08'),
(34, 'Perez', 'Renzo Dominic', 'D.', '1997-11-04', 'gatid', 'Santa Cruz', 'renzoperez@yahoo.com', '09641243184', 'renzoperez@yahoo.com', '86480fa522a48c1b8df9302ac7c06f10', '2018-04-26 02:43:35'),
(35, 'Villarica', 'Mia', 'V.', '1980-08-17', 'Pila', 'Santa Cruz', 'mia.villarica@gmail.com', '09175039034', 'mia.villarica@gmail.com', '86480fa522a48c1b8df9302ac7c06f10', '2018-04-26 08:49:57'),
(36, 'Dela cruz', 'Kim', 'D.', '1998-07-03', 'Magdalena, Laguna', 'Alaminos', 'kimdelacruz@yahoo.com', '09234685213', 'kimdelacruz@yahoo.com', '41d65a82af3580db23fe60e7df2ad575', '2018-05-17 10:32:26');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_laguna`
--

CREATE TABLE IF NOT EXISTS `tbl_laguna` (
  `ID` int(11) NOT NULL,
  `CityTown` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_laguna`
--

INSERT INTO `tbl_laguna` (`ID`, `CityTown`) VALUES
(1, 'Alaminos'),
(2, 'Bay'),
(3, 'Binan'),
(4, 'Cabuyao'),
(5, 'Calamba'),
(6, 'Calauan'),
(7, 'Cavinti'),
(8, 'Famy'),
(9, 'Kalayaan'),
(10, 'Liliw'),
(11, 'Los Banos'),
(12, 'Luisiana'),
(13, 'Lumban'),
(14, 'Mabitac'),
(15, 'Magdalena'),
(16, 'Majayjay'),
(17, 'Nargcarlan'),
(18, 'Paete'),
(19, 'Pagsanjan'),
(20, 'Pakil'),
(21, 'Pangil'),
(22, 'Pila'),
(23, 'Rizal'),
(24, 'San Pablo'),
(25, 'San Pedro'),
(26, 'Santa Cruz'),
(27, 'Santa Maria'),
(28, 'Santa Rosa'),
(29, 'Siniloan'),
(30, 'Victoria');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_logs`
--

CREATE TABLE IF NOT EXISTS `tbl_logs` (
  `ID` int(11) NOT NULL,
  `ProviderID` int(11) NOT NULL,
  `ClientID` int(11) NOT NULL,
  `Action` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `LogDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_logs`
--

INSERT INTO `tbl_logs` (`ID`, `ProviderID`, `ClientID`, `Action`, `LogDate`) VALUES
(2, 1, 1, 'Request for an appointment.', '2018-04-24 07:09:43'),
(4, 1, 1, 'Appointment has been cancelled.', '2018-04-24 07:12:11'),
(5, 1, 4, 'Appointment has been declined.', '2018-04-24 07:12:57'),
(6, 1, 1, 'Appointment has been approved.', '2018-04-24 07:13:16'),
(7, 1, 1, 'Appointment has been completed.', '2018-04-24 07:13:40'),
(8, 7, 4, 'Requested for an appointment.', '2018-04-24 14:45:41'),
(9, 7, 4, 'Appointment has been approved.', '2018-04-24 14:48:24'),
(10, 7, 4, 'Appointment has been approved.', '2018-04-24 14:48:26'),
(11, 7, 4, 'Appointment has been completed.', '2018-04-24 14:55:53'),
(12, 20, 34, 'Requested for an appointment.', '2018-04-26 02:47:10'),
(13, 20, 34, 'Appointment has been completed.', '2018-04-26 02:49:51'),
(14, 20, 35, 'Requested for an appointment.', '2018-04-26 08:55:39'),
(15, 20, 35, 'Appointment has been approved.', '2018-04-26 08:57:43'),
(16, 20, 35, 'Appointment has been approved.', '2018-04-26 08:58:35'),
(17, 7, 36, 'Requested for an appointment.', '2018-05-17 10:37:34');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_profile`
--

CREATE TABLE IF NOT EXISTS `tbl_profile` (
  `ID` int(11) NOT NULL,
  `ClientID` int(11) NOT NULL,
  `Description` text,
  `Image` text
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_profile`
--

INSERT INTO `tbl_profile` (`ID`, `ClientID`, `Description`, `Image`) VALUES
(1, 1, 'Sample Description', 'default.png'),
(2, 2, NULL, 'default.png'),
(3, 3, 'Any plumbing services.', 'default.png'),
(4, 4, 'Any Carpentry Service', 'default.png'),
(6, 5, 'Long year of experience in different type of repairs in Electronic and Appliances.', 'default.png'),
(9, 6, 'Lahat ng tungkol sa computer', 'default.png'),
(11, 7, 'Maninira ng bahay', 'default.png'),
(12, 8, 'Maninira ng bahay', 'default.png'),
(14, 9, 'yyy', 'default.png'),
(15, 10, 'hehe', 'default.png'),
(16, 11, 'Tagasira ng Computer', 'default.png'),
(17, 12, 'Taga ayos ng Tv, Electric Fan', 'default.png'),
(21, 13, 'tanggal n kuko', 'default.png'),
(23, 14, 'Any Carpentry Services.', 'default.png'),
(24, 15, 'mangangain ng kahoy', 'default.png'),
(25, 16, 'plumber', 'default.png'),
(26, 17, 'carpenter', 'default.png'),
(27, 18, 'carpenter', 'default.png'),
(28, 19, 'carpentero', 'default.png'),
(29, 20, 'electrician', 'default.png'),
(30, 21, 'Can fix About Computers', 'default.png'),
(31, 22, 'electrician', 'default.png'),
(32, 23, 'electrician', 'default.png');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_reviews`
--

CREATE TABLE IF NOT EXISTS `tbl_reviews` (
  `ID` int(11) NOT NULL,
  `AppointmentID` int(11) NOT NULL,
  `Review` text NOT NULL,
  `Rating` varchar(10) NOT NULL,
  `DateReviewed` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_reviews`
--

INSERT INTO `tbl_reviews` (`ID`, `AppointmentID`, `Review`, `Rating`, `DateReviewed`) VALUES
(1, 5, 'Great Service. Thumbs up!', '5', '2018-04-16 09:10:32'),
(3, 7, 'Late in appointment.', '3', '2018-04-16 10:01:10'),
(4, 6, 'Sample Review', '5', '2018-04-17 07:47:36'),
(5, 10, 'Excellent service.', '5', '2018-04-22 03:18:23'),
(6, 11, 'thanks', '5', '2018-04-22 04:28:00'),
(7, 15, 'Magaling at mabilis gumawa good job!!', '5', '2018-04-24 15:17:00'),
(8, 16, 'good service', '4', '2018-04-26 02:51:54');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_serviceproviders`
--

CREATE TABLE IF NOT EXISTS `tbl_serviceproviders` (
  `ProviderID` int(11) NOT NULL,
  `Lastname` varchar(50) NOT NULL,
  `Firstname` varchar(50) NOT NULL,
  `Middlename` varchar(50) NOT NULL,
  `Birthday` date NOT NULL,
  `Address` text NOT NULL,
  `CityTown` varchar(50) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `ContactNo` varchar(20) NOT NULL,
  `RecoveryEmail` varchar(255) NOT NULL,
  `Password` varchar(32) NOT NULL,
  `Status` varchar(10) NOT NULL,
  `ServiceID` int(11) NOT NULL,
  `DateRegistered` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_serviceproviders`
--

INSERT INTO `tbl_serviceproviders` (`ProviderID`, `Lastname`, `Firstname`, `Middlename`, `Birthday`, `Address`, `CityTown`, `Email`, `ContactNo`, `RecoveryEmail`, `Password`, `Status`, `ServiceID`, `DateRegistered`) VALUES
(1, 'Valencia', 'Arken', 'Dela Rosa', '1990-01-01', '4000 Paseo de Escudero', 'San Pablo', 'arkensky2266@gmail.com', '09123456789', 'arkensky2266@gmail.com', 'dd4b21e9ef71e1291183a46b913ae6f2', 'Approved', 6, '2018-04-06 11:44:05'),
(2, 'Dela Cruz', 'Juan', 'Reyes', '1990-05-01', '263 National Highway, Canlalay', 'Binan', 'jrdc@gmail.com', '09152664843', 'jrdc@gmail.com', '4297f44b13955235245b2497399d7a93', 'Approved', 2, '2018-04-08 15:57:29'),
(3, 'Sotto', 'James ', 'Manabat', '1980-02-01', '176 National Highway, San Vicente', 'Alaminos', 'sotto.james@gmail.com', '09152664843', 'sotto.james@gmail.com', '4297f44b13955235245b2497399d7a93', 'Approved', 2, '2018-04-08 15:59:02'),
(4, 'laron', 'sky', 'santiago', '1990-01-01', '372 New Solid Bldg.', 'Cavinti', 'sky@gmail.com', '09152664843', 'sky@gmail.com', 'dd4b21e9ef71e1291183a46b913ae6f2', 'Approved', 4, '2018-04-17 02:27:54'),
(5, 'Bona', 'Yumi', 'Gargamel', '1995-09-06', 'Sample Address', 'Famy', 'gargamel@gmail.com', '09152664843', 'gargamel@gmail.com', 'dd4b21e9ef71e1291183a46b913ae6f2', 'Approved', 5, '2018-04-17 07:45:57'),
(6, 'Sumilang', 'Lawrence', 'Rebong', '1998-03-27', 'T. Franco St Victoria Laguna', 'Victoria', 'lrsumilang@gmail.com', '09355240255', 'lrsumilang@gmail.com', 'f187936bd2a13ca4489367590fe2e20e', 'Approved', 6, '2018-04-22 03:41:09'),
(7, 'Caronan', 'Jairhon', 'Almera', '1998-02-12', 'Brgy. Monserrat Oyot', 'Santa Cruz', 'jairhoncaronan@gmail.com', '09269813499', 'jairhoncaronan@gmail.com', '8aa94da6c485cac9cd9a161a686de09e', 'Approved', 4, '2018-04-22 03:53:23'),
(9, 'dog', 'butch', 'the', '1998-06-04', 'brgy san juan', 'Santa Cruz', 'butchthedog@yahoo.com', '09284613243', 'butchthedog@yahoo.com', '467eac5d0f8fb480d01e788321017df5', 'Approved', 1, '2018-04-23 02:43:33'),
(11, 'Tanawan', 'Joseph', 'Onggoy', '1997-06-24', 'Brgy. San Juan', 'Alaminos', 'josephdarwin@gmail.com', '09269813499', 'josephdarwin@gmail.com', 'bd9fa9edbeff8f0b88a6f26ce7665953', 'Approved', 6, '2018-04-23 06:30:31'),
(12, 'Sumilang', 'Lawrence', 'Rebong', '1998-03-27', 'T. Franco St. Victoria, Laguna', 'Victoria', 'lrsumilang123@gmail.com', '09355240255', 'lrsumilang123@gmail.com', 'f9328996f349ffeda9f11a2bc4fc8924', 'Approved', 5, '2018-04-23 06:43:11'),
(13, 'rosh', 'rosh', 'rosh', '1997-08-22', 'liko liko sa tabe', 'Santa Cruz', 'rosh@gmail.com', '09269531425', 'rosh@gmail.com', '5f4dcc3b5aa765d61d8327deb882cf99', 'Approved', 7, '2018-04-23 08:15:29'),
(14, 'Santiago', 'Rafael', 'Laron', '1995-03-26', 'Brgy Vicente', 'Binan', 'arkensky31@gmail.com', '09152664843', 'arkensky31@gmail.com', 'dd4b21e9ef71e1291183a46b913ae6f2', 'Declined', 4, '2018-04-24 06:15:47'),
(15, 'Palaming', 'John Carlo', 'Onggoy', '1999-05-15', 'Brgy. Bagumbayan', 'Santa Cruz', 'jcpalaming@gmail.com', '09265217662', 'jcpalaming@gmail.com', '8ce6a93295bcd99d91fbd534c1903487', 'Pending', 4, '2018-04-24 07:37:12'),
(16, 'Tanawan', 'Jose', 'O.', '1996-02-02', 'brgy.bubukal', 'Santa Cruz', 'josephdarwin_08@yahoo.com', '09312942163', 'josephdarwin_08@yahoo.com', '42edfd179eb9650ee1281ab4884e8cb8', 'Approved', 2, '2018-04-25 16:57:43'),
(17, 'Cole', 'Michael', 'V.', '1996-01-01', 'brgy.  bubukal', 'Santa Cruz', 'josephmole_08@yahoo.com', '09316243191', 'josephmole_08@yahoo.com', '1bbd886460827015e5d605ed44252251', 'Declined', 4, '2018-04-25 17:03:01'),
(18, 'Cole', 'Kira', 'V.', '1996-02-01', 'brgy. bubukal', 'Santa Cruz', 'thekira@yahoo.com', '09321456321', 'thekira@yahoo.com', '0eb878301a984ff7b2641adc2ac3531e', 'Declined', 4, '2018-04-25 17:05:52'),
(19, 'Layson', 'Laurence', 'P.', '1986-04-09', 'brgy.bubukal', 'Santa Cruz', 'laurencelayson000@yahoo.com', '09346197245', 'laurencelayson000@yahoo.com', '86480fa522a48c1b8df9302ac7c06f10', 'Pending', 4, '2018-04-26 02:30:25'),
(20, 'Daran', 'Dick', 'N.', '1978-12-06', 'brgy.pagsawitan', 'Alaminos', 'dickdaran@yahoo.com', '09341625791', 'dickdaran@yahoo.com', '86480fa522a48c1b8df9302ac7c06f10', 'Approved', 1, '2018-04-26 02:40:14'),
(21, 'Bernardino', 'Mark', 'P.', '1992-05-23', 'Brgy. Monserrat', 'Alaminos', 'mark.bernardino@gmail.com', '09279813499', 'mark.bernardino@gmail.com', '6d295738eb6579053ac46a9ca1902583', 'Approved', 6, '2018-04-26 08:51:07'),
(22, 'butch', 'bubuy', 'd', '1996-04-08', 'Pagsanjan, Laguna', 'Santa Cruz', 'bubuy@yahoo.com', '09346284915', 'bubuy@yahoo.com', '41d65a82af3580db23fe60e7df2ad575', 'Pending', 1, '2018-05-17 10:44:41'),
(23, 'Wayne', 'Bruce', 'O', '1990-08-08', 'brgy.san jose', 'Santa Cruz', 'butch@yahoo.com', '09635421568', 'butch@yahoo.com', '41d65a82af3580db23fe60e7df2ad575', 'Pending', 1, '2018-05-17 10:50:09');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_services`
--

CREATE TABLE IF NOT EXISTS `tbl_services` (
  `ID` int(11) NOT NULL,
  `Service` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_services`
--

INSERT INTO `tbl_services` (`ID`, `Service`) VALUES
(1, 'Electrical Services'),
(2, 'Plumbing Services'),
(3, 'Pest Control Services'),
(4, 'Carpentry Services'),
(5, 'Electronic and Appliances Repair Services'),
(6, 'Computer Repair and IT Services'),
(7, 'Welding'),
(8, 'Maintenance Services'),
(9, 'Construction and building inspectors'),
(10, 'Pipefitting'),
(11, 'Parlorista'),
(12, 'Manicurist');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_appointments`
--
ALTER TABLE `tbl_appointments`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_clients`
--
ALTER TABLE `tbl_clients`
  ADD PRIMARY KEY (`ClientID`);

--
-- Indexes for table `tbl_laguna`
--
ALTER TABLE `tbl_laguna`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_logs`
--
ALTER TABLE `tbl_logs`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_profile`
--
ALTER TABLE `tbl_profile`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_reviews`
--
ALTER TABLE `tbl_reviews`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_serviceproviders`
--
ALTER TABLE `tbl_serviceproviders`
  ADD PRIMARY KEY (`ProviderID`);

--
-- Indexes for table `tbl_services`
--
ALTER TABLE `tbl_services`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_appointments`
--
ALTER TABLE `tbl_appointments`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `tbl_clients`
--
ALTER TABLE `tbl_clients`
  MODIFY `ClientID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `tbl_laguna`
--
ALTER TABLE `tbl_laguna`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `tbl_logs`
--
ALTER TABLE `tbl_logs`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `tbl_profile`
--
ALTER TABLE `tbl_profile`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `tbl_reviews`
--
ALTER TABLE `tbl_reviews`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tbl_serviceproviders`
--
ALTER TABLE `tbl_serviceproviders`
  MODIFY `ProviderID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `tbl_services`
--
ALTER TABLE `tbl_services`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

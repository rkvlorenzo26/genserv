<?php
include_once 'include_once/connection.php';
include_once 'include_once/header.php';
?>

  <body>

  <section id="container" >
      <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
      <!--header start-->
      
      <?php include_once 'include_once/navbar.php' ?>
      
      <!-- header end -->
      
      <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
      <!--sidebar start-->
      
      <?php include_once 'include_once/sidebar.php'; ?>
      
      <!--sidebar end-->
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper site-min-height">
          	<h3><i class="fa fa-angle-right"></i> Service Providers</h3>
            
            <?php  
            if(isset($_GET['message'])){
                $message = $_GET['message'];
                
                if($message == 'approved_success'){
                ?>
                    <div class="alert alert-info alert-dismissible" id="success-alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo "Successfully approved a service provider"; ?>
                    </div>  
                <?php
                }
            } 
            ?>
              
              
            <div class="row mt">
            <div class="col-lg-12">
            <h4>Pending Service Providers</h4>  
            
            <table id="example2" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th width="2%">ID</th>
                            <th width="25%">Name</th>    
                            <th width="20%">Address</th>
                            <th width="10%">City/Town</th>
                            <th width="10%">Email Address</th>
                            <th width="10%">Contact No.</th>
                            <th>Service</th>
                            <th width="10%">Date Registered</th>
                            <th>Action</th>
                         </tr>
                        </thead>
                        <tbody>
                            <?php
                                
                            $q = mysqli_query($con,"Select * from tbl_serviceproviders where Status = 'Pending'");
                                
                            while($row = mysqli_fetch_array($q)){
                                $service = mysqli_query($con,"Select * from tbl_services where ID = '$row[ServiceID]'");
                                $serv = mysqli_fetch_array($service);
                            ?>
                                <tr>
                                <td><?php echo $row['ProviderID']; ?></td>
                                <td><?php echo $row['Lastname'].', '.$row['Firstname'].' '.$row['Middlename']; ?></td>
                                <td><?php echo $row['Address'];?></td>
                                <td><?php echo $row['CityTown']; ?></td>
                                <td><?php echo $row['Email']; ?></td>
                                <td><?php echo $row['ContactNo']; ?></td>
                                <td><?php echo $serv['Service'];?></td>
                                <td><?php echo $row['DateRegistered']; ?></td>
                                <td><center><button class="btn btn-success btn-xs" data-toggle="modal" data-target="<?php echo "#".$row['ProviderID'] ?>"><i class="fa fa-check"></i> Approve</button></center></td>
                                </tr>
                            
                                <div class="modal fade" id="<?php echo $row['ProviderID'] ?>">
                                    <div class="modal-dialog modal-md">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button class="close" data-dismiss="modal">×</button>
                                                <h4 class="modal-title">Approve Service Provider</h4>
                                            </div><!-- end modal-header -->
                                            <div class="modal-body">
                                                <form method="POST" action="function/provider-approve.php">
                                                    <center>
                                                    <input type="hidden" name="id" value="<?php echo $row['ProviderID']; ?>"
                                                    <div class="form-group">
                                                    <p>Are you sure that you want to approve the selected service provider?</p>
                                                    </div>
                                                    <div class="form-group">
                                                    <center>    
                                                        <button type="submit" name="approve" class="btn btn-primary">Yes</button>
                                                        <button data-dismiss="modal" class="btn btn-primary">No</button>
                                                    </center>        
                                                    </div>
                                                    </center>
                                                </form>     
                                            </div><!-- end modal-content -->
                                        </div><!-- end modal-dialog -->
                                    </div>
                                </div>  
                            
                            
                                
                            <?php
                            }
                            ?>
                        </tbody>
                    </table>    
                
                
                
                
                
            </div>
              
          	
          		<div class="col-lg-12">
                    <h4>Approved Service Providers</h4>  
          		    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th width="2%">ID</th>
                            <th width="25%">Name</th>    
                            <th width="20%">Address</th>
                            <th width="10%">City/Town</th>
                            <th width="10%">Email Address</th>
                            <th width="10%">Contact No.</th>
                            <th>Service</th>
                            <th width="10%">Date Registered</th>
                         </tr>
                        </thead>
                        <tbody>
                            <?php
                                
                            $query = mysqli_query($con,"Select * from tbl_serviceproviders where Status = 'Approved'");
                                
                            while($row = mysqli_fetch_array($query)){
                                $service = mysqli_query($con,"Select * from tbl_services where ID = '$row[ServiceID]'");
                                $serv = mysqli_fetch_array($service);
                            ?>
                                <tr>
                                <td><?php echo $row['ProviderID']; ?></td>
                                <td><?php echo $row['Lastname'].', '.$row['Firstname'].' '.$row['Middlename']; ?></td>
                                <td><?php echo $row['Address'];?></td>
                                <td><?php echo $row['CityTown']; ?></td>
                                <td><?php echo $row['Email']; ?></td>
                                <td><?php echo $row['ContactNo']; ?></td>
                                <td><?php echo $serv['Service'];?></td>
                                <td><?php echo $row['DateRegistered']; ?></td>
                                </tr>
                                
                            <?php
                            }
                            ?>
                        </tbody>
                    </table>
          		</div>
          	</div>
			
		</section><! --/wrapper -->
      </section><!-- /MAIN CONTENT -->

      <!--main content end-->
      
      <!--footer start-->
      <?php
      include_once 'include_once/footer.php';
      ?>
      <!--footer end-->
      
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
    <?php include_once 'include_once/js.php'; ?>

  </body>
</html>
<?php
include_once 'include_once/connection.php';
include_once 'include_once/header.php';
?>

  <script src="reports/assets/js/jquery-1.12.4.min.js"></script>
  <script src="reports/assets/canvasjs.min.js"></script>

  <body>

  <section id="container" >
      <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
      <!--header start-->
      
      <?php include_once 'include_once/navbar.php' ?>
      
      <!-- header end -->
      
      <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
      <!--sidebar start-->
      
      <?php include_once 'include_once/sidebar.php'; ?>
      
      <!--sidebar end-->
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper site-min-height">
          	<h3><i class="fa fa-angle-right"></i> Reports</h3>
          	<div class="row mt">
          		<div class="col-lg-6">
                    <div id="chartAppointment"></div>
                    <?php
                        $services = mysqli_query($con,"Select ServiceID, count(ServiceID) as count from tbl_appointments GROUP BY ServiceID");
                        $dataPoints = array(); 
                        while($row = mysqli_fetch_array($services)){
                            $query = mysqli_query($con,"Select * from tbl_services where ID = '$row[ServiceID]'");
                            $a = mysqli_fetch_array($query);

                            $arr = array("y" => $row['count'], "label" => $a['Service']);
                            array_push($dataPoints,$arr);
                        }
                    ?>
                    <script type="text/javascript">

                                $(function () {
                                    var chart = new CanvasJS.Chart("chartAppointment", {
                                        theme: "theme2",
                                        animationEnabled: true,
                                        exportFileName: "Service appointment frequency",
                                        title: {
                                            text: "Appointments based on services"
                                        },
                                        data: [
                                        {
                                            type: "pie",           
                                            dataPoints: <?php echo json_encode($dataPoints, JSON_NUMERIC_CHECK); ?>
                                        }
                                        ]
                                    });
                                    chart.render();
                                });
                    </script>
              </div>

              <div class="col-lg-6">
                    <div id="chartAppointmentPerMonth"></div>
                    <?php
                        $services = mysqli_query($con,"Select MONTHNAME(DateRequest) as Month, count(*) as Total from tbl_appointments where YEAR(DateRequest) = 2018 GROUP BY MONTHNAME(DateRequest)");
                        $dataPoints = array(); 
                        while($row = mysqli_fetch_array($services)){
                            $arr = array("y" => $row['Total'], "label" => $row['Month']);
                            array_push($dataPoints,$arr);
                        }
                    ?>
                    <script type="text/javascript">

                                $(function () {
                                    var chart = new CanvasJS.Chart("chartAppointmentPerMonth", {
                                        theme: "theme2",
                                        animationEnabled: true,
                                        exportFileName: "Appointment frequency",
                                        title: {
                                            text: "Appointment frequency per month"
                                        },
                                        data: [
                                        {
                                            type: "pie",                     
                                            dataPoints: <?php echo json_encode($dataPoints, JSON_NUMERIC_CHECK); ?>
                                        }
                                        ]
                                    });
                                    chart.render();
                                });
                    </script>
              </div>
                
                <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
                <div class="col-lg-6">
                    <div id="chartClients"></div>
                    <?php
                        $q = mysqli_query($con,"Select MONTHNAME(DateRegistered) as Month, count(*) as Total from tbl_clients where YEAR(DateRegistered) = 2018 GROUP BY MONTHNAME(DateRegistered)");
                        $dataPoints = array(); 
                        while($row = mysqli_fetch_array($q)){
                            $arr = array("y" => $row['Total'], "label" => $row['Month']);
                            array_push($dataPoints,$arr);
                        }
                    ?>
                    <script type="text/javascript">

                                $(function () {
                                    var chart = new CanvasJS.Chart("chartClients", {
                                        theme: "theme2",
                                        animationEnabled: true,
                                        exportFileName: "No. of Registered Clients per month (2018)",
                                        title: {
                                            text: "No. of Registered Clients per month (2018)"
                                        },
                                        data: [
                                        {
                                            type: "column",                
                                            dataPoints: <?php echo json_encode($dataPoints, JSON_NUMERIC_CHECK); ?>
                                        }
                                        ]
                                    });
                                    chart.render();
                                });
                    </script>
                </div>

                <div class="col-lg-6">
                    <div id="chartServiceProviders"></div>
                    <?php
                        $q = mysqli_query($con,"Select MONTHNAME(DateRegistered) as Month, count(*) as Total from tbl_serviceproviders where YEAR(DateRegistered) = 2018 GROUP BY MONTHNAME(DateRegistered)");
                        $dataPoints = array(); 
                        while($row = mysqli_fetch_array($q)){
                            $arr = array("y" => $row['Total'], "label" => $row['Month']);
                            array_push($dataPoints,$arr);
                        }
                    ?>
                    <script type="text/javascript">

                                $(function () {
                                    var chart = new CanvasJS.Chart("chartServiceProviders", {
                                        theme: "theme2",
                                        animationEnabled: true,
                                        exportFileName: "Service appointment frequency",
                                        title: {
                                            text: "No. of Registered Providers per month (2018)"
                                        },
                                        data: [
                                        {
                                            type: "column",                
                                            dataPoints: <?php echo json_encode($dataPoints, JSON_NUMERIC_CHECK); ?>
                                        }
                                        ]
                                    });
                                    chart.render();
                                });
                    </script>
                </div>

          		</div>
          	</div>
			
		</section><! --/wrapper -->
      </section><!-- /MAIN CONTENT -->

      <!--main content end-->
      
      <!--footer start-->
 
      <!--footer end-->
      
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
    <?php include_once 'include_once/js.php'; ?>

  </body>
</html>

<?php
include_once 'include_once/connection.php';
session_start();
$err = "";

if(isset($_POST['submit']))
{
    $user = $_POST['user'];
    $pass = md5($_POST['pass']);
    
    $query = mysqli_query($con,"Select * from tbl_admin where Username = '$user' and Password = '$pass'");
    $count = mysqli_num_rows($query);
    
    if($count != 0)
    {
        $_SESSION['adminloggedin'] = true;
        header('location: index.php');
    }
    else
    {
          $err = "<div class='alert alert-danger'>Incorrect Username or Password!</div>";
    }
}
else if(isset($_SESSION['adminloggedin']))
{
    header('location: index.php');
}
?>

<!DOCTYPE html>
<html>
<head>
	<title>Laguna - GENSERV</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
	<link href="login/big-logo.png" rel="shortcut icon" />
	<link href="login/admin-login.css" rel="stylesheet" type="text/css"/>
	<link href="login/font-awesome-4.5.0/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="icon" href="assets/logo.png">
</head>
    
</style>
<body>
	<div class="container">
		<img src="assets/logo.png" style="width: 170px; height: 170px;" />
        <br><h3>LOGIN</h3>
		<div id="loginmessage"><?php echo $err; ?></div>
		<form id="formLogin" method="POST">
			<div class="form-input">
				<input type="text" name="user" placeholder="Username" autocomplete="off"/>
			</div>
			<div class="form-input">
				<input type="password" name="pass" placeholder="Password" autocomplete="off" />
			</div>
			<button class="btn-login" type="submit" name="submit"><i class="fa fa-sign-in"></i> Login</button>
		</form>
	</div>
	<footer>
	<center>
	<p class="foot">© 2018 Laguna - GENSERV. All rights reserved.</p>
	</center>
	</footer>
</body>
</html>